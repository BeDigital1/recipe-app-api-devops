resource "aws_s3_bucket" "app_public_files" {
  bucket        = "${local.prefix}-recipe-app-api-devop-b6q8odtcaj6-files"
  acl           = "public-read"
  force_destroy = true
}
